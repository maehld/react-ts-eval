import { Reducer, Action, createStore, Store } from 'redux';

export type State = {
    user: string,
    message: string,
};

const initialState = {
    user: 'Dominik',
    message: 'Hallo Welt',
};

const reducer: Reducer<State> = (state: State, action: Action): State => {
    switch (action.type) {
        case 'CHANGE_MESSAGE':
            return {
                ...state,
                message: (action as ChangeMessageAction).newMessage
            };
        default:
            return state;
    }
};

export const store: Store<State> = createStore<State>(reducer, initialState);

interface ChangeMessageAction extends Action {
    newMessage: string;
};

export const changeMessage = (newMessage: string): ChangeMessageAction => {
    return {
        type: 'CHANGE_MESSAGE',
        newMessage,
    };
};
