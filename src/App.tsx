import * as React from 'react';
import './App.css';
import { connect } from 'react-redux';
import { State, changeMessage } from './store';

const logo = require('./logo.svg');

interface AppProps {
  user: string;
  message: string;
  changeMessage(newMessage: string): {};
}

interface AppState {
  newMessage: string;
}

class App extends React.Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);
    this.state = {
      newMessage: ''
    };
  }

  onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.setState((prevState) => {
      this.props.changeMessage(prevState.newMessage);      
      return {
        newMessage: ''
      };
    });
  }

  onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newMessage: e.currentTarget.value
    });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React, hello {this.props.user} and here's your message: {this.props.message}</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.tsx</code> and save to reload.
        </p>
        If you submit, the next message would be: {this.state.newMessage}
        <form onSubmit={this.onSubmit}>
          <input type="text" onChange={this.onChange}/>
          <input type="submit"/>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state: State): Partial<AppProps> => {
  return {
    user: state.user,
    message: state.message,
  };
};

const mapDispatchToProps = {
  changeMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
